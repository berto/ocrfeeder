ocrfeeder (0.8.1-4) unstable; urgency=medium

  * Don't require python-gobject (Closes: #890160).
    - debian/control: remove the build dependency.
    - skip-python-gobject.patch: remove the check during configure.
  * debian/copyright:
    - Update copyright years.
    - Use HTTPS for the format URL.

 -- Alberto Garcia <berto@igalia.com>  Tue, 13 Feb 2018 17:48:08 +0200

ocrfeeder (0.8.1-3) unstable; urgency=medium

  * plain-text-unicode.patch:
    - Fix unicode problems when exporting to plain text (Closes: #811571).
  * ocrfeeder-cli.patch:
    - Add missing options to the ocrfeeder-cli manpage (Closes: #813496).
  * debian/control:
    - Bump Standards-Version to 3.9.8 (no changes needed).
    - Add build dependency on dh-python and python-setuptools.
    - Use https url in the Vcs-Browser field.
    - Update homepage url.
    - Bump build dependency on debhelper to 9.
  * debian/compat:
    - Set compat version to 9.
  * debian/copyright:
    - Update copyright years.

 -- Alberto Garcia <berto@igalia.com>  Mon, 02 May 2016 17:32:32 +0300

ocrfeeder (0.8.1-2) unstable; urgency=medium

  * Upload to unstable (Closes: #717576, #708745, #740150).
  * debian/copyright: Update copyright years:

 -- Alberto Garcia <berto@igalia.com>  Sun, 16 Aug 2015 23:12:59 -0700

ocrfeeder (0.8.1-1) experimental; urgency=medium

  * New upstream release.
  * gdk-threads.patch:
    - Remove.

 -- Alberto Garcia <berto@igalia.com>  Fri, 26 Dec 2014 16:27:32 +0100

ocrfeeder (0.8-2) experimental; urgency=medium

  * gdk-threads.patch:
    - Fix usage of the gdk_threads API (Closes: #766554).
  * desktop-exec-code.patch:
    - Add exec code to .desktop file, fixes desktop-mime-but-no-exec-code.
  * debian/control:
    - Bump Standards-Version to 3.9.6 (no changes needed).

 -- Alberto Garcia <berto@igalia.com>  Fri, 24 Oct 2014 10:51:17 +0300

ocrfeeder (0.8-1) experimental; urgency=low

  * New upstream release. This version is ported to GObject Introspection
    and no longer uses the old python bindings of some libraries.
  * debian/control:
    - Replace python-pygoocanvas with gir1.2-goocanvas-2.0.
    - Replace python-gtk2 with gir1.2-gtk-3.0.
    - Replace python-imaging-sane with python-pil and python-sane.
    - Add dependencies on python-gi, gir1.2-gtkspell3-3.0 and iso-codes.
    - Add build dependencies on python-gi and python-gobject.
    - Bump Standards-Version to 3.9.5 (no changes needed).
  * debian/patches/gtkspell.patch:
    - Remove, this patch is no longer needed.
  * debian/rules:
    - Set XDG_RUNTIME_DIR to /tmp, otherwise configure will fail.

 -- Alberto Garcia <berto@igalia.com>  Thu, 07 Aug 2014 00:52:46 +0300

ocrfeeder (0.7.11-3) unstable; urgency=low

  * Update my e-mail address in debian/control and debian/copyright.
  * Add headers to all patches and remove their numbering.
  * gtkspell.patch: remove deprecated GtkSpell support (Closes: #707849).
    - debian/rules: run dh --with autoreconf.
    - debian/control: add build dependency on dh-autoreconf and
      libglib2.0-dev (for the AM_GLIB_GNU_GETTEXT macro) and remove
      dependencies on python-gtkspell.
  * automake-warnings.patch: set automake strictness to foreign.
  * debian/control: remove build dependency on python-gtk2-dev. This is
    not necessary since OCRFeeder 0.7.4.

 -- Alberto Garcia <berto@igalia.com>  Sat, 08 Jun 2013 01:54:11 +0200

ocrfeeder (0.7.11-2) unstable; urgency=low

  * Upload to unstable.

 -- Alberto Garcia <agarcia@igalia.com>  Sun, 05 May 2013 21:45:32 +0300

ocrfeeder (0.7.11-1) experimental; urgency=low

  * New upstream release.
  * debian/copyright: Update e-mail address and copyright years.
  * debian/control:
    - Remove obsolete DM-Upload-Allowed flag.
    - Bump Standards-Version to 3.9.4 (no changes needed).
    - Use canonical URLs in Vcs-* fields.
    - Depend on python-lxml.

 -- Alberto Garcia <agarcia@igalia.com>  Fri, 08 Feb 2013 10:47:37 +0000

ocrfeeder (0.7.10-1) experimental; urgency=low

  * New upstream release.

 -- Alberto Garcia <agarcia@igalia.com>  Thu, 11 Oct 2012 23:18:59 +0300

ocrfeeder (0.7.9-1) unstable; urgency=low

  * New upstream release (Closes: #661499).
  * debian/copyright: update format URL to version 1.0.
  * debian/copyright: add copyright information for Debian files.
  * Drop 03_missing-help-file.patch, it's already in this release.

 -- Alberto Garcia <agarcia@igalia.com>  Wed, 11 Apr 2012 22:26:53 +0300

ocrfeeder (0.7.7-2) unstable; urgency=low

  * debian/control: 
    - Drop python-gnome2 dependency as version 0.7.7 no longer requires it
      (Closes: #661346).
    - Bump Standards-Version to 3.9.3 (no changes needed).
    - Add myself to Uploaders.

 -- Bernhard Reiter <ockham@raz.or.at>  Sun, 26 Feb 2012 21:49:49 +0100

ocrfeeder (0.7.7-1) unstable; urgency=low

  * New upstream release (Closes: #646605).
  * debian/watch: scan for .xz files, upstream no longer uses bz2.
  * debian/copyright: rewrite using the machine-readable format.
  * debian/control: add build dependency on python-reportlab.
  * 03_missing-help-file.patch: upstream forgot to add this file.

 -- Alberto Garcia <agarcia@igalia.com>  Sat, 10 Dec 2011 18:21:56 +0200

ocrfeeder (0.7.6-1) unstable; urgency=low

  * New upstream release.
  * Add dependency on python-reportlab.
  * Move to dh_python2:
    - Replace python-support with python-all (>= 2.6.6-3~).
    - Use '--with python2' in debian/rules.
    - Replace debian/pyversions with X-Python-Version in debian/control.

 -- Alberto Garcia <agarcia@igalia.com>  Sat, 06 Aug 2011 11:46:34 +0200

ocrfeeder (0.7.5-1) unstable; urgency=low

  * New upstream release.
  * debian/control: add build dependency on python-imaging-sane.
  * debian/control: update Standards-Version to 3.9.2.

 -- Alberto Garcia <agarcia@igalia.com>  Thu, 19 May 2011 15:31:39 +0300

ocrfeeder (0.7.4-1) unstable; urgency=low

  * New upstream release.
  * Drop the following patches, now obsolete after upstream changes:
    - 03_unpaper.patch (upstream 396df7).
    - 04_gtkspell.patch (upstream e5348d).
  * debian/control: add build dependency on python-enchant,
    python-gtkspell and python-pygoocanvas.
  * debian/control: add dependency on ghostscript, needed to import PDF
    files.

 -- Alberto Garcia <agarcia@igalia.com>  Sun, 20 Mar 2011 11:06:56 +0200

ocrfeeder (0.7.3-1) unstable; urgency=low

  * New upstream release.
  * Add debian/watch file.
  * debian/rules: switch from cdbs to dh.
  * debian/copyright: the source tarball is again the same as upstream's
    since the freeware icon has been replaced with a GPLed one.
  * 01_icon-path.patch: add description and update icon path.
  * Drop the following patches, now obsolete after upstream changes:
    - 02_ocrfeeder-package.patch (upstream 92980f).
    - 03_openoffice-icon.patch (upstream 544e74).
    - 04_ocrfeeder-cli-engines-fix.patch (upstream 646523).
    - 05_ocrfeeder-cli-help.patch (upstream 646523).
  * New patches:
    - 02_potfiles-quilt.patch: make intltool-update skip quilt's .pc
      directory.
    - 03_unpaper.patch: don't break the config dialog if unpaper is not
      installed (upstream 6b8976).
    - 04_gtkspell.patch: catch GTKSpell exceptions when a dictionary is
      not found (upstream e5348d).
  * debian/control: fix GTK+ name in package description.
  * debian/control: add DM-Upload-Allowed flag.
  * debian/control: update Standards-Version to 3.9.1.
  * debian/control: add build dependencies on python-gtk2-dev and
    gnome-doc-utils; remove cdbs and python.
  * debian/control: add dependencies on python-imaging-sane,
    python-enchant, python-gtkspell and cuneiform; remove python-imaging.
  * debian/control: remove unnecessary version numbers from dependencies.
  * debian/control: recommend 'yelp' to browse the online help.

 -- Alberto Garcia <agarcia@igalia.com>  Tue, 08 Feb 2011 11:05:55 +0100

ocrfeeder (0.6.6+dfsg1-1) unstable; urgency=low

  * New version based on upstream 0.6.6 with the freeware icon
    odf/thumbnail.py removed from the source package (Closes: #598627)
  * debian/copyright: explain the icon removal from the source tarball
  * debian/control: Add myself to Uploaders
  * 03_openoffice-icon.patch: Update patch
  * 05_ocrfeeder-cli-help.patch: Add '--help' option to ocrfeeder-cli and
    update man page (Closes: #581426)

 -- Alberto Garcia <agarcia@igalia.com>  Fri, 08 Oct 2010 14:21:52 +0200

ocrfeeder (0.6.6-3) unstable; urgency=low

  * Team upload
  * debian/control: Depend on python-gnome2 (Closes: #580254)
  * 04_ocrfeeder-cli-engines-fix.patch: Fix IndexError (Closes: #581427)

 -- Serafeim Zanikolas <sez@debian.org>  Thu, 13 May 2010 00:25:03 +0200

ocrfeeder (0.6.6-2) unstable; urgency=low

  * 02_ocrfeeder-package.patch: Place all python files inside the
    'ocrfeeder' package (Closes: #578426, #579008)
  * 03_openoffice-icon.patch: Include the latest version of the
    OpenOffice.org icon, which is GPLed
  * debian/copyright: Add copyright information for the new icon
  * debian/control: Add dependency on an OCR engine (tesseract-ocr, ocrad
    or gocr)
  * debian/control: Recommend the unpaper package

 -- Alberto Garcia <agarcia@igalia.com>  Tue, 20 Apr 2010 21:51:07 +0100

ocrfeeder (0.6.6-1) unstable; urgency=low

  * Initial release (Closes: #568155)
  * 01_icon-path.patch: Set full path of OCRFeeder icon

 -- Alberto Garcia <agarcia@igalia.com>  Mon, 05 Apr 2010 20:02:04 +0200
