<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="projects" xml:lang="gl">

<info>
    <link type="guide" xref="index#projects"/>
    <desc>Cargar e gardar proxectos</desc>
</info>

<title>Proxectos</title>

<p>A miúdo, un usuario pode querer gardar o traballo realizado até o momento nunha imaxe e continuar con él maís tarde. Para este caso <app>OCRFeeder</app> ofrece a posibilidade de gardar e cargar proxectos.</p>

<p>Os proxectos son ficheiros comprimidos coa extensión <em>ocrf</em> que reteñen información sobre as páxinas (imaxes) e áreas de contido.</p>

<section>
<title>Gardando un proxecto</title>

<p>After having done some work in an image, a project can be created by clicking <guiseq><gui>File</gui><gui>Save</gui></guiseq> or <guiseq><gui>File</gui><gui>Save As…</gui></guiseq>. Opcionalmente, pódense usar os atallos de teclado <keyseq><key>Control</key><key>G</key></keyseq> ou <keyseq><key>Control</key><key>Shift</key><key>G</key></keyseq>. Aparecerá unha caixa de diálogo para gardar o ficheiro e deberáse introducir o nome e a localización do proxecto.</p>

</section>

<section>
<title>Cargando un proxecto</title>

<p>Pódese cargar un proxecto xa existente premendo <guiseq><gui>Ficheiro</gui><gui>Abrir</gui></guiseq> ou  <keyseq><key>Control</key><key>A</key></keyseq>.</p>

</section>

<section>
<title>Engadindo un proxecto</title>

<p>Ás veces é útil combinar dous ou máis proxectos, co fin de crear un único documento que contén as páxinas de varios proxectos <app>OCRFeeder</app>. Isto pódese facer engadindo un proxecto, que simplemente carga as páxinas desde o proxecto elexido no actual. Para facelo, prema en <guiseq><gui>Ficheiro</gui><gui>Engadir proxecto</gui></guiseq> e escolla o proxecto que quere.</p>

</section>

<section>
<title>Eliminando un proxecto</title>

<p>Se toda a información nun proxecto debe ser eliminada (por exemplo, para comezar de novo), pódese facer escollendo <guiseq><gui>Editar</gui><gui>Eliminar proxecto</gui></guiseq>.</p>

</section>


</page>
