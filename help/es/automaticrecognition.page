<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="automaticrecognition" xml:lang="es">

<info>
    <link type="guide" xref="index#recognition"/>
    <link type="seealso" xref="addingimage"/>
    <desc>Reconocer automáticamente una imagen</desc>
</info>

<title>Reconocimiento automático</title>

<p><app>OCRFeeder</app> intenta detectar los contenidos en la imagen de un documento y realizar un OCR sobre ellos, distinguiendo también qué son gráficos y qué es texto. Para simplificar este concepto, lo llamaremos «reconocimiento».</p>

<p>Después de añadir una imagen, esta se puede reconocer automáticamente pulsando <guiseq><gui>Documento</gui><gui>Reconocer documento</gui></guiseq>.</p>

<note style="important"><p>Ya que hay diferentes distribuciones de documentos, el reconocimiento automático, sobre todo la segmentación de la página, puede no ser precisa para su documento. En este caso, puede ser necesaria un poco de edición manual de los resultados del reconocimiento.</p></note>

<note style="warning"><p>El reconocimiento automática realiza algunas operaciones complejas y pueden llevar algún tiempo, dependiendo del tamaño de la imagen y de la complejidad del diseño.</p>
<p>El reconocimiento automático reemplazará todas las áreas de contenido en la página seleccionada actualmente.</p></note>

</page>
