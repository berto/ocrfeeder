<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="projects" xml:lang="cs">

<info>
    <link type="guide" xref="index#projects"/>
    <desc>Načítání a ukládání projektů</desc>
</info>

<title>Projekty</title>

<p>Někdy může uživatel chtít uložit stav práce na obrázku a pokračovat v ní později. Pro tyto případy nabízí aplikace <app>OCRFeeder</app> možnost projekty ukládat a načítat.</p>

<p>Projekty jsou komprimované soubory s příponou <em>ocrf</em>, které uchovávají informace o stránkách (obrázcích) a oblastech s obsahem.</p>

<section>
<title>Ukládání projektu</title>

<p>Po dokončení nějakých prací na obrázku je možné uložit projekt kliknutím na <guiseq><gui>Soubor</gui><gui>Uložit</gui></guiseq> nebo <guiseq><gui>Soubor</gui><gui>Uložit jako…</gui></guiseq>. Případně lze použít klávesových zkratek <keyseq><key>Control</key><key>S</key></keyseq> nebo <keyseq><key>Control</key><key>Shift</key><key>S</key></keyseq>. Objeví se dialogové okno ukládání, do kterého se zadá název a umístění projektu.</p>

</section>

<section>
<title>Načítání projektu</title>

<p>Existující projekt lze načíst jednoduše kliknutím na <guiseq><gui>Soubor</gui><gui>Otevřít</gui></guiseq> nebo zmáčknutím <keyseq><key>Control</key><key>O</key></keyseq>.</p>

</section>

<section>
<title>Připojování projektu</title>

<p>Někdy může být užitečné sloučit dohromady dva nebo více projektů, aby z nich vznikl dokument s více stránkami z několika projektů <app>OCRFeeder</app>. To lze udělat připojením projektu, kdy se prostě načtou stránky ze zvoleného projektu do aktuálního projektu. Pokud to chcete, klikněte na <guiseq><gui>Soubor</gui><gui>Připojit projekt</gui></guiseq> a zvolte ten, který chcete připojit.</p>

</section>

<section>
<title>Mazání projektu</title>

<p>Když je potřeba všechny informace v projektu vymazat (například, když chcete začít od znovu), můžete tak učinit volbou <guiseq><gui>Upravit</gui><gui>Vymazat projekt</gui></guiseq>.</p>

</section>


</page>
