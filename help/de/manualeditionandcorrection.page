<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="manualeditionandcorrection" xml:lang="de">

<info>
    <link type="guide" xref="index#recognition"/>
    <link type="seealso" xref="addingimage"/>
    <link type="seealso" xref="automaticrecognition"/>
    <desc>Manuelle Bearbeitung und Korrektur der Ergebnisse</desc>
</info>

<title>Manuelle Bearbeitung</title>

<p>Gelegentlich möchten Sie nur einen bestimmten Bereich eines Bildes einlesen lassen oder die Ergebnisse der automatischen Erkennung nachträglich korrigieren. In <app>OCRFeeder</app> können Sie jeden Aspekt des Inhalts eines Dokuments auf einfache Weise beeinflussen.</p>

<section>

<title>Inhaltsbereiche</title>

<p>Die erwähnten Inhalte des Dokuments werden durch Bereiche dargestellt, wie im folgenden Bild:</p>
<media type="image" mime="image/png" src="figures/content-areas.png" width="300px">Ein Bild mit zwei Inhaltsbereichen, wovon einer ausgewählt ist.</media>

<p>Die Attribute eines ausgewählten Bereiches können an der rechten Seite des Hauptfensters bearbeitet werden, wie im folgenden Bild gezeigt:</p>
<media type="image" mime="image/png" src="figures/areas-edition.png" width="200px">Bild, das die Benutzeroberfläche zum Bearbeiten von Inhaltsbereichen zeigt</media>

<p>Die folgende Liste zeigt die Attribute für Inhaltsbereiche:</p>
<list>
    <item><p><em>Typ</em>: Setzt den Typ des Bereichs auf Bild oder Text. Falls es ein Bild ist, wird dieser Bereich aus der Originalseite unverändert in das erzeugte Dokument übernommen. Falls es Text ist, wird der Bereich wiederum als Text im erzeugten Dokument dargestellt. Erstellte ODT-Dokumente erhalten hierfür Textrahmen, falls Bereiche auf den Typ <em>Text</em> gesetzt wurden.</p></item>
    <item><p><em>Beschneiden</em>: Zeigt den aktuellen Ausschnitt aus dem Originalbereich. Dadurch ist es für den Benutzer einfacher nachzuvollziehen, was sich innerhalb des Bereiches befindet.</p></item>
    <item><p><em>Grenzen</em>: Zeigt die Koordinaten X und Y der oberen linken Ecke des Bereiches im Originalbild sowie die Breite und Höhe des Bereiches an.</p></item>
    <item><p><em>OCR-Anwendung</em>: Hier können Sie eine OCR-Anwendung auswählen und den Text des ausgewählten Bereiches durch Anklicken von <gui>OCR</gui> erkennen lassen.</p>. <note type="warning"><p>Der Aufruf der OCR-Anwendung zur Texterkennung ordnet diesen Text diesem Bereich zu und ersetzt vorher zugewiesene Texte.</p></note></item>
    <item><p><em>Textbereich</em>: Repräsentiert den Text, der diesem Bereich zugeordnet ist und ermöglicht Ihnen, den Text zu bearbeiten. Dieser Bereich ist deaktiviert, wenn der Inhaltstyp auf <em>Bild</em> gesetzt ist.</p></item>
    <item><p><em>Stil</em>: Hier können Sie den Typ und die Größe der Schrift auswählen, weiterhin die Textausrichtung, den Zeilenabstand und die Laufweite der Schrift.</p></item>
</list>

<p>Die Inhaltsbereiche können durch Anklicken ausgewählt werden oder über das Menü <guiseq><gui>Dokument</gui><gui>Vorherigen Bereich auswählen</gui></guiseq> und <guiseq><gui>Dokument</gui><gui>Nächsten Bereich auswählen</gui></guiseq>. Alternativ können Sie die Tastenkombinationen <keyseq><key>Strg</key><key>Umschalttaste</key><key>P</key></keyseq> und <keyseq><key>Strg</key><key>Umschalttaste</key><key>N</key></keyseq> verwenden.</p>

<p>Außerdem können Sie alle Bereiche auswählen, indem Sie <guiseq><gui>Dokument</gui><gui>Alle Bereiche auswählen</gui></guiseq> wählen oder die Tastenkombination <keyseq><key>Strg</key><key>Umschalttaste</key><key>A</key></keyseq> drücken.</p>

<p>Sofern mindestens ein Inhaltsbereich ausgewählt ist, können Sie dessen Inhalt automatisch erkennen oder löschen lassen. Wählen Sie hierzu <guiseq><gui>Dokument</gui><gui>Ausgewählte Bereiche berücksichtigen</gui></guiseq> und <guiseq><gui>Dokument</gui><gui>Ausgewählte Bereiche löschen</gui></guiseq> oder drücken Sie die Tastenkombinationen <keyseq><key>Strg</key><key>Umschattaste</key><key>Entf</key></keyseq>).</p>

</section>

</page>
